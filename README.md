# Tinusaur Shield LEDx2 Documentation

Choose from the list below.

## Tinusaur Shield LEDx2, Gen:4 mk-III - Assembling Guide

[![Tinusaur Shield LEDx2 Assembling Guide](Tinusaur_Shield_LEDx2_Gen4_mkIII_Assembling_Guide.jpg)](https://gitlab.com/tinusaur/shield-ledx2-docs/-/raw/master/Tinusaur_Shield_LEDx2_Gen4_mkIII_Assembling_Guide.pdf?inline=false)


- Download link: https://gitlab.com/tinusaur/shield-ledx2-docs/-/raw/master/Tinusaur_Shield_LEDx2_Gen4_mkIII_Assembling_Guide.pdf?inline=false


## Tinusaur Shield LEDx2 Gen:4 mk-I - Assembling Guide.

- [Tinusaur_Shield_LEDx2_Gen4_mkI_Assembling_Guide.pdf](Tinusaur_Shield_LEDx2_Gen4_mkI_Assembling_Guide.pdf)
- Download link at GitLab: https://gitlab.com/tinusaur/shield-ledx2-docs/-/raw/master/Tinusaur_Shield_LEDx2_Gen4_mkI_Assembling_Guide.pdf?inline=false

## Tinusaur Shield LEDx2 Gen:3 - Assembling Guide.

- [Tinusaur_Shield_LEDx2_Gen3_Assembling_Guide.pdf](Tinusaur_Shield_LEDx2_Gen3_Assembling_Guide.pdf)
- Download link at GitLab: https://gitlab.com/tinusaur/shield-ledx2-docs/-/raw/master/Tinusaur_Shield_LEDx2_Gen3_Assembling_Guide.pdf?inline=false

